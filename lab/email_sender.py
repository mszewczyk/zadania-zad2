# -*- encoding: utf-8 -*-

import smtplib
import email.utils

def prompt(prompt):
    return raw_input(prompt).strip()


def create_and_send_email():
    # Odczyt danych od użytkownika
    print("Witaj w prostym kliencie mailowym.\r\n")
    from_addr = prompt("Od:")
    to_addrs = prompt("Do:")
    subject = prompt("Tytul:")
    message = prompt("Tresc wiadomosci:")
    template = "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n"
    headers = template % (from_addr, to_addrs, subject)
    email_body = headers + message



    try:
        # Połączenie z serwerem pocztowym
        server = smtplib.SMTP('194.29.175.240', 25)
        # Ustawienie parametrów
        server.set_debuglevel(True)
        # Autentykacja
        server.starttls()
        server.ehlo()
        #login = prompt("Podaj login: ")
        #password = prompt("Podaj haslo: ")
        server.login("p8","magicznehaslo")
        # Wysłanie wiadomości
        server.sendmail(from_addr, [to_addrs, ], email_body)
        pass

    finally:
        # Zamknięcie połączenia
        server.close()
        pass

decision = 't'

while decision in ['t', 'T', 'y', 'Y']:
    create_and_send_email()
    decision = raw_input('Wysłać jeszcze jeden list? ')
