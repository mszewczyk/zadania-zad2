# -*- encoding: utf-8 -*-
import os
from rfc822 import formatdate

import socket
import string

pages = { "/": "web/web_page.html","/test": "web/test.html", "/error":"web/error.html" }
def http_serve(server_socket, host):
    while True:
        # Czekanie na polaczenie
        connection, client_address = server_socket.accept()
        msg="BLAD"
        try:
            # Odebranie zadania
            request = connection.recv(1024)
            if request:
                print "Odebrano: "
                print request
                lines = string.split(request, '\r\n')
                #print lines
                header = string.split(lines[0], ' ')
                if header[0] == "GET" and header[2] == "HTTP/1.1":
                    print "URI: http://"+host+header[1] #heroku wysyła tylko do serwera ['GET / HTTP/1.1', ''], więc musze sobie host sam wziąć
                    gmtTime = formatdate() #data w formacie rfc-1123
                    httpCode = "HTTP/1.1 200 OK"
                    if pages.has_key(header[1]): #sprawdzam czy taka podstrona jest dostepna, jesli nie to wyrzucam error page
                        html = open(pages[header[1]]).read()
                    else:
                        httpCode = "HTTP/1.1 404 Not Found"
                        html = open(pages["/error"]).read()
                    msg = httpCode+"\r\nContent-Type: text/html; charset=UTF-8; \r\nDate: "+gmtTime+";\r\n\r\n" + html
                    # wyslanie zawartosci strony
                connection.sendall(msg)
        finally:
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego uzycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

host = '0.0.0.0' #http://damp-plateau-2708.herokuapp.com/
port = int(os.getenv('PORT'))
# powiazanie gniazda z adresem
server_address = (host, port)
server_socket.bind(server_address)

# nasluchiwanie przychodzacych polaczen
print("Serwer wystartował ("+host+":"+str(port)+"), nasłuchiwanie.")
print
server_socket.listen(1)

try:
    http_serve(server_socket, host)

finally:
    server_socket.close()

